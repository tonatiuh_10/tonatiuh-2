print('Hola bienvenido')
print('Selecciona el calculo que quieres hacer, escribiendo su número')
print('1. Calcular la hipotenusa de un triángulo rectángulo')
print('2. Calcular el área de un triángulo')
print('3. Calcular el perímetro de un triángulo')
print('4. Calcular el área de un circulo')
print('5. Calcular el diámetro de un circulo')
print('6. Calcular el volumen de una esfera')

número = int(input('¿Qué opción quieres?: '))

while 1:

    if número==1:
      #Calculo de la hipotenusa.

      #Entrada de datos.
      num1 = float(input('Ingresa el valor del cateto a: '))
      num2 = float(input('Ingresa el valor del cateto b: '))    
      n = int(input('¿Cuántos decimales quieres dejar?: '))
  
      #Calculo.
      hipo = ((num1**2) + (num2**2))**(1/2)
  
      #Impresíón datos.
      print('La hipotenusa es: ', (round(hipo, n)), 'metros')

    elif número==2:
      #Calculo del área de un triángulo.

      #Entrada de datos.
      b = float(input('Ingresa el valor de la base: '))
      a = float(input('Ingresa el valor de la altura: '))   
      n = int(input('¿Cuántos decimales quieres dejar?: '))

      #Calculo.
      area = (b*a)/2

      #Impresión de datos.
      print('El área del triangulo es: ', (round(area, n)), 'metros cuadrados')

    elif número==3:
      #Calculo del perímetro de un triángulo.

      #Entrada de datos.
      lado1=float(input('Ingresa el valor de lado 1: '))
      lado2=float(input('Ingresa el valor de lado 2: ')) 
      lado3=float(input('Ingresa el valor de lado 3: ')) 
      n = int(input('¿Cuántos decimales quieres dejar?: '))

      #Calculo.
      perimetro = (lado1+lado2+lado3)

      #Impresión de datos.
      print('El perímetro del triangulo es: ', (round(perimetro, n)), 'metros')

    elif número==4:
      #Calculo del área de un circulo.

      #Entrada de datos.
      from math import pi 

      r = float(input('Ingresa el valor del radio: '))
      n = int(input('¿Cuántos decimales quieres dejar?: '))

      #Calculo.
      area = pi*(r**2)

      #Impresion de datos.
      print('El area del circulo es: ', (round(area, n)), 'metros cuadrados')

    elif número==5:
      #Calculo del diametro de un ciculo.

      #Entrada de datos.
      r = float(input('Ingresa el valor del radio: '))
      n = int(input('¿Cuántos decimales quieres dejar?: '))

      #Calculo.
      diametro = (r*2)

      #Impresion de datos.
      print('El diámetro del circulo es: ', (round(diametro, n)), 'metros')

    elif número==6:
      #Calculo del volumen de una esfera.

      #Entrada de datos.
      from math import pi 

      r = float(input('Ingresa el valor del radio: '))
      n = int(input('¿Cuántos decimales quieres dejar?: '))

      #Calculo.
      volumen = (4/3) * (pi*(r**3)) 

      #Entrada de datos.
      print('El volumen de la esfera es: ', (round(volumen, n)), 'unidades cubicas')






